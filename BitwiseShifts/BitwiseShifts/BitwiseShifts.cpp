// BitwiseShifts.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

int main()
{
	int number;

	std::cout << "Enter the number: ";
	std::cin >> number;

		for (int i=15;i>=0;i--)
		{
			if ((1 << i) & number)
				std::cout << "1";
			else
				std::cout << "0";
		}

	std::cout << "\n";
	system("pause");

    return 0;
}